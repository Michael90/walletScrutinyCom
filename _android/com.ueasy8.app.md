---
wsId: 
title: UEEx
altTitle: 
authors:
- danny
users: 5000
appId: com.ueasy8.app
appCountry: 
released: 2021-12-28
updated: 2023-09-11
version: 4.1.4
stars: 
ratings: 
reviews: 
size: 
website: 
repository: 
issue: 
icon: com.ueasy8.app.png
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-08-17
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: janelezvzzmm
features: 

---

## App Description from Google Play

> UEEx is a digital currency trading platform with the fastest way to buy digital currency, the best transaction rates, and the most reliable security.
>
> Easily buy and sell BTC, ETH, DOGE, and other digital assets

## Analysis 

- Upon initial inspection, it looks like a cryptocurrency exchange
- We verified our email address and found what appears to be a BTC wallet. 
- Upon closer inspection, we found out that it was BTC on the OMNI blockchain or BSC. This is not real bitcoin. 
- The app does **not support BTC** on the BTC blockchain.
