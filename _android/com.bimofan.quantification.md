---
wsId: 
title: AI Trade-BTC,ETH.. Trading Bot
altTitle: 
authors:
- danny
users: 1000
appId: com.bimofan.quantification
appCountry: 
released: 2021-11-09
updated: 2023-06-26
version: 3.2.2
stars: 3.2
ratings: 
reviews: 3
size: 
website: 
repository: 
issue: 
icon: com.bimofan.quantification.png
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-08-16
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: AITrade
features: 

---

## App Description from Google Play

> AI Trade is a trading bot for BTC, ETH, Doge and other cryptocurrencies.
>
> The latest AI code is used to train high-frequency trading strategies suitable for cryptocurrencies.
>
> Just set a few simple parameters, you can achieve more than 100% daily income.
>
> This is where high frequency and high leverage trading gets interesting.

## Analysis 

- We installed the app and did not find any BTC wallet. 
- As the description states, this is a trading bot designed for users to bind their exchange account's API.
- There is **no wallet**.