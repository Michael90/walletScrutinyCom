---
wsId: 
title: 'Conio: Wallet Bitcoin & Crypto'
altTitle: 
authors:
- leo
users: 50000
appId: com.conio.wallet
appCountry: 
released: 
updated: 2023-11-04
version: 7.0.0
stars: 
ratings: 
reviews: 
size: 
website: https://www.conio.com
repository: 
issue: 
icon: com.conio.wallet.png
bugbounty: 
meta: ok
verdict: custodial
date: 2020-04-09
signer: 
reviewArchive: 
twitter: conio
social:
- https://www.linkedin.com/company/conio
- https://www.facebook.com/ConioHQ
redirect_from:
- /com.conio.wallet/
- /posts/com.conio.wallet/
developerName: Conio Inc.
features: 

---

This app has wonderful security claims on Google Play:

> *5. STAY CAREFREE* With Conio you can recover your Bitcoins even if you forget
> all your credentials! Just create your profile and upload a selfie when you
> start.

Unfortunately this means that the provider does control the keys which makes it
a custodial app. Our verdict: **not verifiable**.
