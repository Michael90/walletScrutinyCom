---
wsId: 
title: FinanceX - Crypto Trading with
altTitle: 
authors:
- danny
users: 1000
appId: com.financex
appCountry: 
released: 2019-01-15
updated: 2020-01-13
version: 1.3.12
stars: 
ratings: 
reviews: 
size: 
website: http://www.financex.io
repository: 
issue: 
icon: com.financex.png
bugbounty: 
meta: obsolete
verdict: custodial
date: 2023-05-25
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: FinanceX
features: 

---

## App Description from Google Play 

> FinanceX ensure all of your information is confidential and money is safely stored in your wallet by using Protection from DDoS attacks, Two-factor authentication (2FA), Withdrawals protection, **Cold wallets**

## Analysis 

- When we registered, we registered with Vinamon
- This looks like a cryptocurrency exchange 
- KYC verification increases users' withdrawal limits
- As a cryptocurrency exchange which makes use of cold wallets, we can say that this app is **custodial.**