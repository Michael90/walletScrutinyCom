---
wsId: taralityWallet
title: 'Tarality: Crypto Trading App'
altTitle: 
authors:
- danny
users: 10000
appId: com.taralwallet
appCountry: 
released: 2021-10-22
updated: 2023-10-30
version: 10.9.0
stars: 3
ratings: 
reviews: 4
size: 
website: 
repository: 
issue: 
icon: com.taralwallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-18
signer: 
reviewArchive: 
twitter: TaralityCoin
social:
- https://www.facebook.com/TaralityCoin
- https://www.linkedin.com/company/tarality
- https://t.me/TaralityCommunity
redirect_from: 
developerName: Tarality Ecosystems Private Limited
features: 

---

## App Description from Google Play

> - With Tarality Wallet, you can manage Bitcoin, Ethereum, BNB token right on your mobile phone.
>
> Main Features
>
> - Instant transfer among app users.
> - Transfers among the app users are instant and do not incur any fees.
> - QR Code Payment System

## Analysis

- We registered, but received an [error](https://twitter.com/BitcoinWalletz/status/1681128714866753536) after inputting a 4-digit pin:

> Server maintenance mode is enabled, please try again later.

- We were not able to inspect the app's features beyond that point. 
- If we were to assume that all the provider's claims are true, then the 'Services' section of the [terms](https://tarality.online/terms-and-conditions/) would prevail:
    - The app generates the private keys, and they have no access to it. This makes it self-custodial.
- We were not able to find any claims regarding source-availability. 
- There were [0 results](https://github.com/search?q=com.taralwallet&type=code) when we searched for the app ID, on GitHub Code.
- Since this app was very recently updated, we couldn't mark it as defunct. For now, the app is **not source-available.**

