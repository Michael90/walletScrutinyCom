---
wsId: nooneWallet
title: Noone Wallet
altTitle: 
authors:
- danny
users: 10000
appId: io.noone.androidwallet
appCountry: 
released: 2023-03-17
updated: 2023-11-03
version: 1.7.1
stars: 4.6
ratings: 
reviews: 2
size: 
website: https://noone.io
repository: 
issue: 
icon: io.noone.androidwallet.png
bugbounty: 
meta: ok
verdict: nosource
date: 2023-06-29
signer: 
reviewArchive: 
twitter: NooneWallet
social: 
redirect_from: 
developerName: NO ONE FZCO
features: 

---

## App Description from Google Play

> With Noone Wallet's non-custodial approach, you remain in full control of your funds
>
> Supported Cryptocurrencies:
>
> Bitcoin (BTC) Legacy and Bitcoin Segwit...

## Analysis

- The app claims that it supports BTC and is non-custodial.
- We verified these claims.
- There are no claims to it being source-available.
- A code search on GitHub [does not yield any result.](https://github.com/search?q=io.noone.androidwallet&type=code)  
- This app is **not source-available**.
