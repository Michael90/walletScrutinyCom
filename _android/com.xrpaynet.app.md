---
wsId: voltaXRPayNet
title: Volta Wallet
altTitle: 
authors:
- danny
users: 5000
appId: com.xrpaynet.app
appCountry: 
released: 2022-07-05
updated: 2023-08-25
version: 0.2.5-prod
stars: 4.5
ratings: 
reviews: 26
size: 
website: 
repository: 
issue: 
icon: com.xrpaynet.app.png
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-07
signer: 
reviewArchive: 
twitter: XRPayNet
social:
- https://xrpaynet.com
- https://www.linkedin.com/company/xrpaynet
- https://www.facebook.com/XRPayNet-107640621785961
- https://t.me/XRPayNetChat
redirect_from: 
developerName: XRPayNet
features: 

---

## App Description from Google Play

> Version 1 app serves as both a secure coin custody wallet and an exchange platform, featuring nearly 300 different cryptocurrencies that can all be seamlessly exchanged with one another. With approximately 90,000 trading pairs available, Volta Wallet offers unparalleled options for diversifying portfolios. Unlike most crypto exchanges, which limit smaller project coins/tokens to just a few trading options like Bitcoin, Ethereum, and USDT, Volta Wallet enables users to exchange any of the 300 coins on our platform with one another

## Analysis

- The app mentions that it provides secure coin **custody**.
- The first time we opened the app, we registered and verified our email address.
- It then gave us the opportunity to choose between a custodial and a non-custodial wallet. We selected a non-custodial wallet.
- It provided us with a 24-word seed phrase, which we then confirmed.
- The app then showed us the main interface.
- We added a BTC wallet, gave it a name and tapped on create a wallet. The app gave a Bech32 BTC address that can send/receive.
- It is possible to swap to other coins.
- This is a **self-custodial** wallet.
- They make no claims regarding source-availability.
- A code search on GitHub for the app ID, yielded [0 results](https://github.com/search?q=com.xrpaynet.app&type=code) but with 1 unrelated commit.
- This app is **not source-available**.
