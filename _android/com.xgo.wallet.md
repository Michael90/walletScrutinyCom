---
wsId: xGoWallet
title: 'XGo: Buy, Swap & Store Crypto'
altTitle: 
authors:
- danny
users: 10000
appId: com.xgo.wallet
appCountry: 
released: 
updated: 2023-11-01
version: 1.11.1
stars: 
ratings: 
reviews: 
size: 
website: https://xgo.com
repository: 
issue: 
icon: com.xgo.wallet.png
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-17
signer: 
reviewArchive: 
twitter: XGo_official
social:
- https://t.me/+Z536yScjjQg0NzE0
- https://www.instagram.com/xgo_official
- https://discord.com/invite/dTt4Ke3
redirect_from: 
developerName: stores@xgo
features: 

---

## App Description from Google Play

- Allows users to buy, sell, swap BTC
- Features functions to send and receive.
- WalletID allows users to bind Metamask, Ledger or Coinbase accounts to the user account.

## Analysis

- The app features a multi-currency account which includes bitcoin.
- Verification is needed before we even get to see the wallet address.
- We did not find any option to backup the private keys.
- Section 4.1 of the [terms](https://support.xgo.com/hc/en-gb/articles/5977815359005-Terms-of-Service), describes the service as having full custody of the user's assets.
- This is a **custodial** provider.
