---
wsId: 
title: Dzengi.com - Crypto Exchange
altTitle: 
authors:
- leo
users: 1000000
appId: com.currency.exchange.prod2
appCountry: 
released: 2019-04-15
updated: 2023-11-07
version: 1.42.0
stars: 
ratings: 
reviews: 
size: 
website: http://dzengi.com
repository: 
issue: 
icon: com.currency.exchange.prod2.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive: 
twitter: currencycom
social:
- https://www.linkedin.com/company/currencycom/
- https://www.facebook.com/currencycom/
redirect_from:
- /com.currency.exchange.prod2/
- /posts/com.currency.exchange.prod2/
developerName: DI INVESTMENTS LLC
features: 

---

This is an interface for a custodial trading platform and thus **not
verifiable**.
