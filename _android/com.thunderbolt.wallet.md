---
wsId: 
title: Thunderbolt Lightning Wallet
altTitle: 
authors: 
users: 1000
appId: com.thunderbolt.wallet
appCountry: 
released: 2021-08-29
updated: 2021-08-29
version: 1.0.3
stars: 4.2
ratings: 
reviews: 37
size: 
website: https://thunderboltwallet.us
repository: 
issue: 
icon: com.thunderbolt.wallet.png
bugbounty: 
meta: obsolete
verdict: fake
date: 2023-08-27
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: EncryptDR
features:
- ln

---

<div class="alertBox"><div>
<p>This app is <a href="https://github.com/btcontract/wallet/issues/146">most likely a scam!</a></p>
</div></div>
