---
wsId: iForexCrypto
title: iFOREX Crypto online trading
altTitle: 
authors:
- danny
users: 1000
appId: iforexcrypto.clients.android
appCountry: 
released: 
updated: 2023-05-28
version: 2.1.46
stars: 
ratings: 
reviews: 
size: 
website: https://www.iforexcrypto.com
repository: 
issue: 
icon: iforexcrypto.clients.android.png
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2023-08-14
signer: 
reviewArchive: 
twitter: iforexcrypto_o
social:
- https://www.facebook.com/iFOREXCrypto
- https://www.instagram.com/iforexcrypto
redirect_from: 
developerName: Formula Investments House LTD
features: 

---

## App Description from Google Play

  > Get a simple, elegant, and secure way to trade bitcoin and other cryptocurrencies without blockchain fees.
  >
  > We allow you to trade cryptocurrencies such as Bitcoin, (BTC), Ether (ETH), Dogecoin (DOGE), Shiba Inu (SHIB), Bitcoin Cash (BCH), Litecoin, Dash, Solana, Chainlink (LINK) and others.
  > 
  > CONTRACTS-IN, WALLETS- OUT
  >
  > Crypto wallets are problematic; long access codes, unique protocols and malicious hackers can seriously damage your actions and investments.
  >
  > Crypto contracts remove these difficulties and give you rapid access to what’s important- the price changes and volatility of the crypto market.

## Analysis 

- This app facilitates the buying and selling of Bitcoin and other cryptocurrencies, without actually using wallets as it explicitly describes. This is a CFD cryptocurrency trading platform which means that it **does not allow users to send or receive bitcoin.**