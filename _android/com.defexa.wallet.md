---
wsId: 
title: Defexa - Safe Crypto Wallet
altTitle: 
authors: 
users: 1000
appId: com.defexa.wallet
appCountry: 
released: 2023-04-28
updated: 2023-10-23
version: 1.2.2
stars: 3.9
ratings: 
reviews: 4
size: 
website: https://defexa.io/wallet/
repository: 
issue: 
icon: com.defexa.wallet.png
bugbounty: 
meta: ok
verdict: wip
date: 2023-06-03
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: FPS Global LTD
features: 

---

