---
wsId: bitcointrade
title: Ripio Trade
altTitle: 
authors:
- danny
appId: com.root.BitcoinTrade
appCountry: br
idd: 1320032339
released: 2017-12-13
updated: 2023-11-06
version: 4.6.9
stars: 3.8
reviews: 960
size: '45360128'
website: http://ripio.com/br/ripiotrade/
repository: 
issue: 
icon: com.root.BitcoinTrade.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-11
signer: 
reviewArchive: 
twitter: 
social:
- https://www.linkedin.com/company/bitcointrade
- https://www.facebook.com/BitcointradeBR
features: 
developerName: PeerTrade Digital

---

{% include copyFromAndroid.html %}
