---
wsId: theKingdomBank
title: The Kingdom Bank
altTitle: 
authors:
- danny
appId: com.kingdom.TheKingdomBank
appCountry: bg
idd: '1626275936'
released: 2022-06-15
updated: 2023-11-01
version: 8.4.9
stars: 0
reviews: 0
size: '36325376'
website: https://portal.thekingdombank.com/signup
repository: 
issue: 
icon: com.kingdom.TheKingdomBank.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-11-02
signer: 
reviewArchive: 
twitter: kingdombankcom
social:
- https://www.linkedin.com/company/thekingdombank
- https://www.facebook.com/thekingdombank
- https://www.instagram.com/thekingdombankcom
- https://www.youtube.com/channel/UCqMayzyXGkITAQsbfiKPowA
- https://t.me/TheKingdomBank_bot
features: 
developerName: The Kingdom Bank Corporation

---

{% include copyFromAndroid.html %}