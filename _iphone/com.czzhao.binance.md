---
wsId: binance
title: 'Binance: Buy Bitcoin & Crypto'
altTitle: 
authors:
- leo
appId: com.czzhao.binance
appCountry: 
idd: 1436799971
released: 2018-10-06
updated: 2023-10-27
version: 2.73.4
stars: 4.7
reviews: 162048
size: '734031872'
website: https://www.binance.com
repository: 
issue: 
icon: com.czzhao.binance.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2020-12-22
signer: 
reviewArchive: 
twitter: binance
social:
- https://www.facebook.com/binance
- https://www.reddit.com/r/binance
features: 
developerName: Binance LTD

---

In the description the provider claims:

> Your funds are protected by our Secure Asset Fund for Users (SAFU Funds) which
  means we have your back.

which sounds very custodial and as such the app is **not verifiable**.
