---
wsId: marSDigitalAssetWallet
title: 'MarS: Digital Asset Wallet'
altTitle: 
authors:
- danny
appId: io.trustverse.mars
appCountry: kr
idd: '1567874624'
released: 2021-08-10
updated: 2022-11-24
version: 2.0.1
stars: 5
reviews: 3
size: '68384768'
website: 
repository: 
issue: 
icon: io.trustverse.mars.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-08-25
signer: 
reviewArchive: 
twitter: TrustVerse
social:
- https://www.trustverse.io
- https://www.facebook.com/trustverseofficial
- https://t.me/trustverse_officialchannel
- https://www.instagram.com/trustverse_official
- https://trustverse-official.medium.com
features: 
developerName: trustverse

---

{% include copyFromAndroid.html %}