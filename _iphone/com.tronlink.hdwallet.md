---
wsId: tronLinkGlobal
title: 'Tronlink: TRX & BTT Wallet'
altTitle: 
authors:
- danny
appId: com.tronlink.hdwallet
appCountry: us
idd: '1453530188'
released: 2019-03-02
updated: 2023-10-20
version: 4.13.12
stars: 4
reviews: 1059
size: '185192448'
website: https://www.tronlink.org
repository: 
issue: 
icon: com.tronlink.hdwallet.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-06-13
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Helix Tech Company Limited

---

{% include copyFromAndroid.html %}