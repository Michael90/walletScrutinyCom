---
wsId: headlineCFD
title: 'Headline.net: Finance & Invest'
altTitle: 
authors:
- danny
appId: com.mc900.mc900
appCountry: us
idd: '1639509598'
released: 2022-09-09
updated: 2023-10-03
version: 2.5.0
stars: 5
reviews: 1
size: '72136704'
website: https://www.headline.net/
repository: 
issue: 
icon: com.mc900.mc900.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-06
signer: 
reviewArchive: 
twitter: 
social:
- https://www.facebook.com/HeadlineTrade
- https://t.me/Headline_Group
features: 
developerName: Magic Compass Ltd

---

{% include copyFromAndroid.html %}
