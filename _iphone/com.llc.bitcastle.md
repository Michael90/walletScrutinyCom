---
wsId: bitcastleTradeCrypto
title: 'bitcastle: Buy & Trade Crypto'
altTitle: 
authors:
- danny
appId: com.llc.bitcastle
appCountry: gb
idd: '1616104862'
released: 2022-07-15
updated: 2023-03-20
version: 1.2.5
stars: 0
reviews: 0
size: '133924864'
website: https://bitcastle.io/
repository: 
issue: 
icon: com.llc.bitcastle.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-10
signer: 
reviewArchive: 
twitter: bit_castle
social:
- https://www.facebook.com/bitcastle.English
- https://t.me/bitcastle_official
features: 
developerName: bitcastle LLC

---

{% include copyFromAndroid.html %}
