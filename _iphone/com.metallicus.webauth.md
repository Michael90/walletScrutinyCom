---
wsId: webAuthProton
title: WebAuth
altTitle: 
authors:
- danny
appId: com.metallicus.webauth
appCountry: us
idd: '1594500069'
released: 2021-12-15
updated: 2023-08-04
version: 2.0.12
stars: 4.8
reviews: 160
size: '39254016'
website: https://www.proton.org/
repository: 
issue: 
icon: com.metallicus.webauth.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-06
signer: 
reviewArchive: 
twitter: protonxpr
social:
- https://www.facebook.com/protonxpr
- https://www.reddit.com/r/ProtonChain
- https://t.me/protonxpr
- https://discord.com/invite/B2QDmgf
features: 
developerName: Metallicus, Inc.

---

{% include copyFromAndroid.html %}
