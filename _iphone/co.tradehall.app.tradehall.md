---
wsId: tradehallCFD
title: Tradehall
altTitle: 
authors:
- danny
appId: co.tradehall.app.tradehall
appCountry: my
idd: '1642488506'
released: 2022-12-20
updated: 2023-03-10
version: 1.0.14
stars: 5
reviews: 3
size: '79450112'
website: 
repository: 
issue: 
icon: co.tradehall.app.tradehall.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2023-09-05
signer: 
reviewArchive: 
twitter: 
social:
- https://www.tradehall.co
- https://www.facebook.com/Tradehall.co
- https://www.instagram.com/tradehall_ltd
features: 
developerName: Tradehall Pty Ltd

---

{% include copyFromAndroid.html %}