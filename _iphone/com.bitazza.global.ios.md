---
wsId: bitazzaGL
title: BTZ
altTitle: 
authors:
- danny
appId: com.bitazza.global.ios
appCountry: th
idd: '1612226119'
released: 2022-04-07
updated: 2023-10-10
version: 3.3.5
stars: 4.1
reviews: 29
size: '168615936'
website: https://www.bitazza.com
repository: 
issue: 
icon: com.bitazza.global.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-03-09
signer: 
reviewArchive: 
twitter: BitazzaGlobal
social:
- https://www.facebook.com/bitazzaglobal
- https://www.linkedin.com/company/bitazza/
- https://t.me/bitazzaglobal
features: 
developerName: Bitazza Company Limited

---

{% include copyFromAndroid.html %}
