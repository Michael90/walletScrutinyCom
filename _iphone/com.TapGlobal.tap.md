---
wsId: tapngo
title: Tap - Buy Bitcoin & Ether
altTitle: 
authors:
- danny
appId: com.TapGlobal.tap
appCountry: gb
idd: 1492263993
released: 2019-12-20
updated: 2023-10-23
version: 3.0.4
stars: 4.6
reviews: 1529
size: '177472512'
website: https://www.withtap.com
repository: 
issue: 
icon: com.TapGlobal.tap.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-15
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Tap Global Limited

---

 {% include copyFromAndroid.html %}
