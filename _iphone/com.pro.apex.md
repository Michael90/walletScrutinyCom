---
wsId: apexProTrade
title: 'ApeX Pro: Trade Crypto'
altTitle: 
authors:
- danny
appId: com.pro.apex
appCountry: us
idd: '1645456064'
released: 2022-09-27
updated: 2023-11-08
version: 1.22.1
stars: 4.3
reviews: 40
size: '78723072'
website: 
repository: 
issue: 
icon: com.pro.apex.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-07-02
signer: 
reviewArchive: 
twitter: OfficialApeXdex
social:
- https://apex.exchange
- https://apexdex.medium.com
- https://discord.com/invite/366Puqavwx
- https://t.me/ApeXdex
features: 
developerName: APEX DAO LLC

---

{% include copyFromAndroid.html %}
