---
wsId: chichapayCrypto
title: ChiChaPay
altTitle: 
authors:
- danny
appId: com.mymetapay.MyMetaPay
appCountry: us
idd: '1619744034'
released: 2022-06-27
updated: 2023-08-17
version: 1.19.0
stars: 5
reviews: 2
size: '34844672'
website: 
repository: 
issue: 
icon: com.mymetapay.MyMetaPay.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-08-15
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: METAPAY TECHNOLOGY PTE. LTD.

---

{% include copyFromAndroid.html %}
