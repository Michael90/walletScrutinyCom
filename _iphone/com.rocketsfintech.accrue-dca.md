---
wsId: accrue
title: 'Accrue: Send. Spend. Save.'
altTitle: 
authors:
- danny
appId: com.rocketsfintech.accrue-dca
appCountry: gh
idd: '1604973055'
released: 2022-01-18
updated: 2023-11-06
version: 2.3.11
stars: 4.4
reviews: 832
size: '54221824'
website: https://useaccrue.com
repository: 
issue: 
icon: com.rocketsfintech.accrue-dca.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-29
signer: 
reviewArchive: 
twitter: useaccrue
social:
- https://www.instagram.com/useaccrue/
features: 
developerName: Accrue DCA Limited

---

{% include copyFromAndroid.html %}