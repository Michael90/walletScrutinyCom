---
wsId: trezorSuiteLite
title: Trezor Suite Lite
altTitle: 
authors:
- danny
appId: io.trezor.suite
appCountry: us
idd: '1631884497'
released: 2023-05-14
updated: 2023-09-20
version: 23.9.1
stars: 3.4
reviews: 22
size: '40842240'
website: https://trezor.io/
repository: 
issue: 
icon: io.trezor.suite.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-07-18
signer: 
reviewArchive: 
twitter: trezor
social:
- https://www.reddit.com/r/TREZOR
- https://www.instagram.com/trezor.io
- https://www.facebook.com/trezor.io
features: 
developerName: Trezor Company s.r.o.

---

{% include copyFromAndroid.html %}
