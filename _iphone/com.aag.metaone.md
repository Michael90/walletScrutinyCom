---
wsId: metaOneNFT
title: 'MetaOne: Web3 Crypto Wallet'
altTitle: 
authors:
- danny
appId: com.aag.metaone
appCountry: us
idd: '1627212812'
released: 2023-01-03
updated: 2023-10-18
version: 4.0.1
stars: 4.9
reviews: 74
size: '41616384'
website: https://getmeta.one/
repository: 
issue: 
icon: com.aag.metaone.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-02
signer: 
reviewArchive: 
twitter: aag_ventures
social:
- https://www.linkedin.com/company/aag-ventures
- https://blog.aag.ventures
- https://t.me/aagventures
- https://www.linkedin.com/company/aag-ventures
- https://www.facebook.com/aagventures
- https://discord.com/invite/aagventures
features: 
developerName: ACHIP & ACHAIR GUILD VENTURES PTE. LTD.

---

{% include copyFromAndroid.html %}
