---
wsId: iSunOne
title: 'iSunOne: USDC Card'
altTitle: 
authors:
- danny
appId: com.tideisun.tidepay
appCountry: us
idd: 1384802533
released: 2018-06-09
updated: 2023-09-26
version: 4.0.3
stars: 4.5
reviews: 8
size: '76815360'
website: https://isun1.com
repository: 
issue: 
icon: com.tideisun.tidepay.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-10
signer: 
reviewArchive: 
twitter: isunone1
social:
- https://www.linkedin.com/company/isunone
- https://www.facebook.com/iSunOne
features: 
developerName: TIDENET LIMITED

---

{% include copyFromAndroid.html %}
