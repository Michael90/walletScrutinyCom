---
wsId: BitcoinLibre
title: Bitcoin Libre
altTitle: 
authors:
- danny
appId: io.bitcoinlibre.app
appCountry: us
idd: 1590680702
released: 2021-10-27
updated: 2023-11-03
version: 3.4.5
stars: 4.4
reviews: 425
size: '37422080'
website: http://bitcoinlibre.io/
repository: 
issue: 
icon: io.bitcoinlibre.app.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-11-11
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: BIZAN BIZ SA

---

{% include copyFromAndroid.html %}
