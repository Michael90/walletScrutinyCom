---
wsId: bynexExchange
title: Bynex
altTitle: 
authors:
- danny
appId: by.erpbel.hermes
appCountry: by
idd: '1629900361'
released: 2023-04-17
updated: 2023-09-11
version: 1.3.1
stars: 3.7
reviews: 12
size: '32961536'
website: https://bynex.io/en
repository: 
issue: 
icon: by.erpbel.hermes.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-08-09
signer: 
reviewArchive: 
twitter: bynexio
social:
- https://www.facebook.com/bynex.io
- https://www.instagram.com/bynex.io
- https://vk.com/bynexio
- https://t.me/bynex_info
- https://www.youtube.com/@bynex4637
features: 
developerName: LLC ERPBEL

---

{% include copyFromAndroid.html %}