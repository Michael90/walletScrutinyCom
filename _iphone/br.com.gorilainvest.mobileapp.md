---
wsId: gorilaInvest
title: 'Gorila: gestão de investimento'
altTitle: 
authors:
- danny
appId: br.com.gorilainvest.mobileapp
appCountry: us
idd: '1447950043'
released: 2019-01-27
updated: 2023-10-26
version: 5.2.3
stars: 4.7
reviews: 226
size: '46938112'
website: https://gorila.com.br/
repository: 
issue: 
icon: br.com.gorilainvest.mobileapp.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2022-06-24
signer: 
reviewArchive: 
twitter: gorilainvest
social:
- https://www.facebook.com/GorilaInvest
- https://www.instagram.com/gorilainvest
- https://www.youtube.com/channel/UCYJiIU0DqDLiPcq8tWecjdw
- https://www.linkedin.com/company/gorila
features: 
developerName: GORILA DESENVOLVIMENTO CUSTOMIZACAO SOFTWARES INVESTIMENTO LTDA

---

{% include copyFromAndroid.html %}
