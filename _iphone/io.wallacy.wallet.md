---
wsId: wallacyCrypto
title: 'Wallacy: Crypto & BTC Wallet'
altTitle: 
authors:
- danny
appId: io.wallacy.wallet
appCountry: us
idd: '6448592576'
released: 2023-07-11
updated: 2023-10-20
version: '1.2'
stars: 4.8
reviews: 6
size: '44791808'
website: https://wallacy.io/
repository: 
issue: 
icon: io.wallacy.wallet.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-08-30
signer: 
reviewArchive: 
twitter: WallacyWallet
social:
- https://www.facebook.com/Wallacy.io
- https://discord.com/invite/QpEHcR3DUW
- https://www.youtube.com/@WallacyWallet
- https://t.me/wallacywallet
features: 
developerName: ACheckin HRM

---

{% include copyFromAndroid.html %}