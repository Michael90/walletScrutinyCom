---
wsId: BitcoinIRA
title: 'Bitcoin IRA: Crypto Retirement'
altTitle: 
authors:
- danny
appId: com.bitcoinira
appCountry: us
idd: 1534638949
released: 2021-06-20
updated: 2023-10-09
version: 1.5.40
stars: 4.5
reviews: 779
size: '54035456'
website: https://bitcoinira.com/
repository: 
issue: 
icon: com.bitcoinira.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-18
signer: 
reviewArchive: 
twitter: bitcoin_ira
social:
- https://www.linkedin.com/company/bitcoinira
- https://www.facebook.com/BitcoinIRA
features: 
developerName: BitcoinIRA

---

{% include copyFromAndroid.html %}
