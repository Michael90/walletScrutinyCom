---
wsId: coinCRED
title: CoinCRED
altTitle: 
authors:
- danny
appId: com.coincred
appCountry: in
idd: '1596466402'
released: 2021-12-31
updated: 2023-09-13
version: 2.0.3
stars: 3.5
reviews: 104
size: '35855360'
website: https://coincred.org/
repository: 
issue: 
icon: com.coincred.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2022-06-27
signer: 
reviewArchive: 
twitter: CoinCRED_
social:
- https://www.facebook.com/CoinCRED.Official/
- https://www.linkedin.com/company/coin-cred/
features: 
developerName: CoinCRED

---

{% include copyFromAndroid.html %}