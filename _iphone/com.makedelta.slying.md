---
wsId: makeDelta
title: 코인미어캣 | 보조지표 알림, 검색 | 코인차트 알람
altTitle: 
authors:
- danny
appId: com.makedelta.slying
appCountry: kr
idd: '1581110050'
released: 2021-08-15
updated: 2023-10-19
version: 7.0.1
stars: 4.8
reviews: 118
size: '34921472'
website: https://www.coinmrk.com/
repository: 
issue: 
icon: com.makedelta.slying.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-06-14
signer: 
reviewArchive: 
twitter: 
social:
- https://www.facebook.com/profile.php?id=100083754173968
- https://www.youtube.com/channel/UCLC_CKhMggklpoHowc6TvNA
features: 
developerName: Make Delta Co., Ltd.

---

{% include copyFromAndroid.html %}
