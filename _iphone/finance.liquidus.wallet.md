---
wsId: liquidusDeFiCrypto
title: Liquidus - DeFi Crypto Wallet
altTitle: 
authors:
- danny
appId: finance.liquidus.wallet
appCountry: pk
idd: '1625544806'
released: 2023-01-25
updated: 2023-09-04
version: 1.0.10
stars: 0
reviews: 0
size: '54330368'
website: https://liquidus.finance/
repository: 
issue: 
icon: finance.liquidus.wallet.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-08-18
signer: 
reviewArchive: 
twitter: LiquidusFinance
social:
- https://t.me/liquidusfinance
- https://discord.com/invite/zfQUjejyRs
features: 
developerName: Liquidus Defi Technology Systems Ltd

---

{% include copyFromAndroid.html %}
