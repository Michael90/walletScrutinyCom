---
wsId: ember
title: Ember Fund - Invest in Crypto
altTitle: 
authors:
- danny
appId: com.emberfund.ember
appCountry: us
idd: 1406211993
released: 2018-08-04
updated: 2023-10-23
version: '33.3'
stars: 4.6
reviews: 1954
size: '146960384'
website: https://emberfund.io/
repository: 
issue: 
icon: com.emberfund.ember.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive: 
twitter: Ember_Fund
social:
- https://github.com/ember-fund
features: 
developerName: Ember Fund LLC

---

{% include copyFromAndroid.html %}
