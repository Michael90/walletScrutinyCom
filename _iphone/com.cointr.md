---
wsId: coinTR
title: 'CoinTR: Bitcoin,Altcoin,Crypto'
altTitle: 
authors:
- danny
appId: com.cointr
appCountry: us
idd: '1637395401'
released: 2022-08-18
updated: 2023-09-06
version: 2.0.5
stars: 5
reviews: 4
size: '124305408'
website: 
repository: 
issue: 
icon: com.cointr.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-08-19
signer: 
reviewArchive: 
twitter: CoinTRTurkiye
social:
- https://t.me/CoinTRTurkiye
- https://www.instagram.com/cointrturkiye
- https://www.facebook.com/profile.php?id=100083113521452
- https://www.youtube.com/channel/UCU2wOPdZ9mT2g3S2_wQcQQw
features: 
developerName: LEY FINANS BILISIM TEKNOLOJI SANAYI VE TICARET LIMITED SIRKETI

---

{% include copyFromAndroid.html %}
