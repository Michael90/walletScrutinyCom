---
wsId: swiftcoin
title: SwiftCoin
altTitle: 
authors:
- danny
appId: io.swiftcoin
appCountry: my
idd: '6443595428'
released: 2023-04-03
updated: 2023-06-09
version: 1.1.6
stars: 5
reviews: 8
size: '30169088'
website: 
repository: 
issue: 
icon: io.swiftcoin.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-08-30
signer: 
reviewArchive: 
twitter: SwiftCoin380
social:
- https://swiftcoin.io
- https://t.me/+bJywMqqVe940NjQ9
- https://www.instagram.com/swiftcointradeio
- https://www.tiktok.com/@swiftcoinio
- https://www.youtube.com/@swiftcoin
- https://www.facebook.com/SwiftCoin.io
features: 
developerName: Swiftcoin Ltd

---

{% include copyFromAndroid.html %}