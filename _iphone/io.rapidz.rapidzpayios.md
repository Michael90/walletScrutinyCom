---
wsId: rapidzPay
title: Rapidz Pay
altTitle: 
authors:
- danny
appId: io.rapidz.rapidzpayios
appCountry: us
idd: '1558420115'
released: 2021-06-29
updated: 2023-10-31
version: 2.9.6
stars: 0
reviews: 0
size: '37955584'
website: https://www.rapidz.io/
repository: 
issue: 
icon: io.rapidz.rapidzpayios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-19
signer: 
reviewArchive: 
twitter: Rapidz_io
social:
- https://t.me/RapidzCommunity
- https://www.instagram.com/rapidz_io
- https://www.facebook.com/Rapidz.io
features: 
developerName: Rapidz Team

---

{% include copyFromAndroid.html %}