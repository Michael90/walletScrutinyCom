---
wsId: elyps
title: 'Elyps: Think out of the Bank'
altTitle: 
authors:
- danny
appId: com.elyps
appCountry: tn
idd: '1493791290'
released: 2020-05-13
updated: 2023-09-21
version: 10.9.0
stars: 5
reviews: 1
size: '74198016'
website: https://elyps.com
repository: 
issue: 
icon: com.elyps.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2023-08-17
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Elyps

---

{% include copyFromAndroid.html %}