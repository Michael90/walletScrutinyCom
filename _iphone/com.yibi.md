---
wsId: yibiExchange
title: YIBI
altTitle: 
authors:
- danny
appId: com.yibi
appCountry: us
idd: '1638288204'
released: 2022-08-12
updated: 2023-03-15
version: 1.1.6
stars: 3
reviews: 2
size: '138608640'
website: 
repository: 
issue: 
icon: com.yibi.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-07
signer: 
reviewArchive: 
twitter: OfficialYibi
social:
- https://yibi.co
- https://t.me/yibioffical00
- https://officialyibi.medium.com
- https://www.reddit.com/r/yibiOffical
- https://www.youtube.com/@YibiOfficial/video
features: 
developerName: YIBI LTD

---

{% include copyFromAndroid.html %}
