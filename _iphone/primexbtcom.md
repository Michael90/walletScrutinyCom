---
wsId: primeXBTCryptoTrading
title: 'PrimeXBT: Trade 100+ markets'
altTitle: 
authors:
- danny
appId: primexbtcom
appCountry: us
idd: '1522267195'
released: 2021-10-20
updated: 2023-11-07
version: 3.0.3
stars: 4.6
reviews: 194
size: '78651392'
website: 
repository: 
issue: 
icon: primexbtcom.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-01
signer: 
reviewArchive: 
twitter: primexbt
social:
- https://primexbt.com
- https://www.facebook.com/primexbt
- https://t.me/PrimeXBT_English
- https://www.reddit.com/r/PrimeXBT
- https://discord.com/invite/yEr8p72pxu
- https://www.youtube.com/channel/UCzH0C03Gy8uHyKr-Y59cwJg
features: 
developerName: Prime XBT Trading Services Ltd.

---

{% include copyFromAndroid.html %}
