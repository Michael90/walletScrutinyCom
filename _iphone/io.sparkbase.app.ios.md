---
wsId: sparkbaseCrypto
title: Sparkbase Crypto Asset Manager
altTitle: 
authors:
- danny
appId: io.sparkbase.app.ios
appCountry: tt
idd: '1629754003'
released: 2022-07-02
updated: 2023-04-01
version: 3.8.8
stars: 0
reviews: 0
size: '46510080'
website: https://www.sparkbase.io
repository: 
issue: 
icon: io.sparkbase.app.ios.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-07-03
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Sparkbase AG

---

{% include copyFromAndroid.html %}
