---
wsId: weltheeWallet
title: Welthee Wallet
altTitle: 
authors:
- danny
appId: com.CapiteraAG.Welthee
appCountry: sg
idd: '1562108720'
released: 2021-07-19
updated: 2023-10-11
version: 5.0.2
stars: 0
reviews: 0
size: '113022976'
website: https://welthee.com
repository: 
issue: 
icon: com.CapiteraAG.Welthee.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-04-13
signer: 
reviewArchive: 
twitter: Welthee
social:
- https://www.linkedin.com/company/welthee
features: 
developerName: Capitera AG

---

{% include copyFromAndroid.html %}

