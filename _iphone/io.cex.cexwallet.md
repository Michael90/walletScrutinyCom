---
wsId: cexCryptoWallet
title: CEX.IO App - Crypto Wallet
altTitle: 
authors:
- danny
appId: io.cex.cexwallet
appCountry: ua
idd: '1575920503'
released: 2021-10-27
updated: 2023-11-01
version: 2.4.1
stars: 5
reviews: 18
size: '104537088'
website: 
repository: 
issue: 
icon: io.cex.cexwallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-30
signer: 
reviewArchive: 
twitter: cex_io
social:
- https://t.me/CEX_IO
- https://www.facebook.com/CEX.IO
- https://www.linkedin.com/company/cex-io
features: 
developerName: CEX.IO LTD

---

{% include copyFromAndroid.html %}
