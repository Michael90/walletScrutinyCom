---
wsId: elDoradoWalletExchange
title: El Dorado - Wallet & P2P
altTitle: 
authors:
- danny
appId: io.eldorado.app.v1
appCountry: ve
idd: '1591303547'
released: 2022-04-20
updated: 2023-11-03
version: 1.3.662
stars: 4.9
reviews: 609
size: '64911360'
website: https://eldorado.io/
repository: 
issue: 
icon: io.eldorado.app.v1.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-18
signer: 
reviewArchive: 
twitter: eldoradoio
social:
- https://www.linkedin.com/company/eldoradoio
- https://www.instagram.com/eldoradoio
- https://t.me/+400YDvBLAXhmOTQx
features: 
developerName: eldorado.io

---

{% include copyFromAndroid.html %}
