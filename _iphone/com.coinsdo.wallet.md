---
wsId: coinsDoCoinWallet
title: 'CoinWallet: BTC Crypto Wallet'
altTitle: 
authors:
- danny
appId: com.coinsdo.wallet
appCountry: us
idd: '1631258517'
released: 2022-07-12
updated: 2023-10-05
version: 1.1.25
stars: 5
reviews: 3
size: '113128448'
website: 
repository: 
issue: 
icon: com.coinsdo.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-24
signer: 
reviewArchive: 
twitter: coinsdogroup
social:
- https://www.facebook.com/CoinsDogroup
- https://www.linkedin.com/company/coinsdo
features: 
developerName: Tecstation Pte Ltd

---

{% include copyFromAndroid.html %}
