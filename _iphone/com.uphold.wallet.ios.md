---
wsId: UpholdbuyandsellBitcoin
title: 'Uphold: Buy BTC, ETH and 260+'
altTitle: 
authors:
- leo
appId: com.uphold.wallet.ios
appCountry: 
idd: 1101145849
released: 2016-04-19
updated: 2023-11-02
version: 5.13.2
stars: 4.6
reviews: 44375
size: '95475712'
website: https://uphold.com
repository: 
issue: 
icon: com.uphold.wallet.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-14
signer: 
reviewArchive: 
twitter: UpholdInc
social:
- https://www.linkedin.com/company/upholdinc
- https://www.facebook.com/UpholdInc
features: 
developerName: Uphold

---

This app appears to be an interface to a custodial trading platform. In the
App Store description we read:

> - Uphold is fully reserved. Unlike banks, we don’t loan out your money. To
    prove it, we publish our holdings in real-time.

If they hold your money, you don't. As a custodial service this app is **not
verifiable**.
