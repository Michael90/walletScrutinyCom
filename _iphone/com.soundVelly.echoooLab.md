---
wsId: echooDeFi
title: 'Echooo: Safe & Earn with DeFi'
altTitle: 
authors:
- danny
appId: com.soundVelly.echoooLab
appCountry: us
idd: '6446883725'
released: 2023-04-22
updated: 2023-10-19
version: 1.7.0
stars: 5
reviews: 6
size: '190681088'
website: http://www.echooo.xyz
repository: 
issue: 
icon: com.soundVelly.echoooLab.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-24
signer: 
reviewArchive: 
twitter: echooo_wallet
social:
- https://t.me/Echooowallet
- https://discord.com/invite/UX26GYAJw4
features: 
developerName: Echooo Labs Pte Ltd

---

{% include copyFromAndroid.html %}