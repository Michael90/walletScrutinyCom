---
wsId: kochureCrypto
title: 'Kochure: Buy and Sell Crypto'
altTitle: 
authors:
- danny
appId: com.kochure.kochure-app
appCountry: ng
idd: '1595574962'
released: 2021-11-22
updated: 2023-07-03
version: 1.0.10
stars: 4.2
reviews: 133
size: '44537856'
website: 
repository: 
issue: 
icon: com.kochure.kochure-app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-08-16
signer: 
reviewArchive: 
twitter: kochureglobal
social:
- https://www.facebook.com/KochureGlobal
- https://www.instagram.com/kochureglobal
features: 
developerName: Kochure Technologies Limited

---

{% include copyFromAndroid.html %}