---
wsId: arkpayBTCETHWallet
title: ArkPay - BTC ETH Wallet
altTitle: 
authors:
- danny
appId: com.zimi.arkpay
appCountry: tw
idd: '1490824690'
released: 2019-12-29
updated: 2023-08-17
version: 2.11.6
stars: 3.1
reviews: 9
size: '34803712'
website: 
repository: 
issue: 
icon: com.zimi.arkpay.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-08-08
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Zimi

---

{% include copyFromAndroid.html %}
