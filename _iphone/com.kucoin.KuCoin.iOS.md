---
wsId: kucoinExchange
title: KuCoin- Buy Bitcoin & Crypto
altTitle: 
authors:
- leo
appId: com.kucoin.KuCoin.iOS
appCountry: 
idd: 1378956601
released: 2018-05-14
updated: 2023-11-08
version: 3.96.0
stars: 4.6
reviews: 21646
size: '287531008'
website: 
repository: 
issue: 
icon: com.kucoin.KuCoin.iOS.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-02-09
signer: 
reviewArchive: 
twitter: KuCoinCom
social:
- https://www.linkedin.com/company/kucoin
- https://www.facebook.com/KuCoinOfficial
- https://www.reddit.com/r/kucoin
features: 
developerName: Kucoin Technology Co.,Ltd.

---

> KuCoin is the most popular bitcoin exchange that you can buy and sell bitcoin
  securely.

This app is the interface to an exchange. Exchanges are all custodial which
makes the app **not verifiable**.
