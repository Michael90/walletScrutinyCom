---
wsId: sekiappCrypto
title: SekiApp
altTitle: 
authors:
- danny
appId: com.wiseki.sekiapp.new
appCountry: ng
idd: '1622624126'
released: 2022-06-22
updated: 2023-11-03
version: 2.0.8
stars: 4.1
reviews: 19
size: '101322752'
website: 
repository: 
issue: 
icon: com.wiseki.sekiapp.new.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-08-14
signer: 
reviewArchive: 
twitter: Seki_APP
social:
- https://www.linkedin.com/company/sekiapp
- https://www.youtube.com/channel/UCHxxbmOdcfyAmXUfutFfBXA
- https://www.instagram.com/seki_app
- https://www.facebook.com/sekiapp
- https://www.tiktok.com/@seki_app
features: 
developerName: Wiseki Technologies Limited

---

{% include copyFromAndroid.html %}