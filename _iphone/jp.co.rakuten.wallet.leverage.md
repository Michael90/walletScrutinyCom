---
wsId: rakutenLeverage
title: 証拠金取引（レバレッジ）アプリ　楽天ウォレットPro
altTitle: 
authors:
- danny
appId: jp.co.rakuten.wallet.leverage
appCountry: jp
idd: '1503110398'
released: 2020-03-25
updated: 2023-02-22
version: 1.2.4
stars: 2.7
reviews: 61
size: '14194688'
website: 
repository: 
issue: 
icon: jp.co.rakuten.wallet.leverage.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-29
signer: 
reviewArchive: 
twitter: Rakuten_Wallet
social: 
features: 
developerName: Rakuten Wallet, Inc.

---

{% include copyFromAndroid.html %}