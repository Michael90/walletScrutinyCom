---
wsId: argent
title: Argent – DeFi in a tap
altTitle: 
authors:
- danny
appId: im.argent.wallet
appCountry: us
idd: 1358741926
released: 2018-10-25
updated: 2023-10-19
version: 4.20.4
stars: 4.6
reviews: 2115
size: '128706560'
website: https://www.argent.xyz
repository: 
issue: 
icon: im.argent.wallet.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2022-01-10
signer: 
reviewArchive: 
twitter: argentHQ
social: 
features: 
developerName: Argent

---

{% include copyFromAndroid.html %}