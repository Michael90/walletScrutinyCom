---
wsId: bitexenGlobal
title: Bitexen Global
altTitle: 
authors:
- danny
appId: com.bitexenglobal.exchangeapp
appCountry: az
idd: '1634643482'
released: 2022-09-30
updated: 2023-10-29
version: '1.6'
stars: 0
reviews: 0
size: '154552320'
website: https://g.bitexen.com
repository: 
issue: 
icon: com.bitexenglobal.exchangeapp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-09-06
signer: 
reviewArchive: 
twitter: bitexenglobal
social:
- https://www.facebook.com/profile.php?id=100086241075019
- https://www.linkedin.com/company/bitexen
- https://t.me/bitexenglobal
features: 
developerName: Bitexen Europe UAB

---

{% include copyFromAndroid.html %}