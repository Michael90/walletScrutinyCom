---
wsId: biteEU
title: BITEEU
altTitle: 
authors:
- danny
appId: com.idea-soft.biteeu
appCountry: gb
idd: '1472991783'
released: 2019-07-24
updated: 2023-10-03
version: 1.6.0
stars: 0
reviews: 0
size: '35821568'
website: http://www.biteeu.com
repository: 
issue: 
icon: com.idea-soft.biteeu.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-10-06
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: BITEEU LTD

---

{% include copyFromAndroid.html %}