---
wsId: voltaXRPayNet
title: Volta Wallet
altTitle: 
authors:
- danny
appId: com.xrpaynet.app
appCountry: us
idd: '1616451007'
released: 2022-07-11
updated: 2023-08-23
version: 0.2.4+128
stars: 3.7
reviews: 15
size: '111941632'
website: https://xrpaynet.com
repository: 
issue: 
icon: com.xrpaynet.app.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-07
signer: 
reviewArchive: 
twitter: XRPayNet
social:
- https://www.linkedin.com/company/xrpaynet
- https://www.facebook.com/XRPayNet-107640621785961
- https://t.me/XRPayNetChat
features: 
developerName: XRPAYNET GLOBAL LIMITED

---

{% include copyFromAndroid.html %}
