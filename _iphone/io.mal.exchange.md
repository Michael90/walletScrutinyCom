---
wsId: malFinance
title: 'Mal.io: Trade Bitcoin,Tokens'
altTitle: 
authors:
- danny
appId: io.mal.exchange
appCountry: us
idd: '6443828002'
released: 2023-02-13
updated: 2023-10-16
version: 1.5.0
stars: 5
reviews: 3
size: '67543040'
website: https://mal.io
repository: 
issue: 
icon: io.mal.exchange.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-11
signer: 
reviewArchive: 
twitter: MalFinance
social:
- https://t.me/mal_io
- https://youtube.com/channel/UCChWLjFpifD4L1oymW0NBZQ
- http://tiktok.com/@mal.io1
features: 
developerName: Mal Network Limited

---

{% include copyFromAndroid.html %}
