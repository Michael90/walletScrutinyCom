---
wsId: fatBTC
title: FatBTC Exchange
altTitle: 
authors:
- danny
appId: com.fatbtc.FatBTCApp
appCountry: us
idd: 1490226195
released: 2019-12-31
updated: 2022-07-25
version: '2.5'
stars: 2.5
reviews: 2
size: '45585408'
website: https://www.fatbtc.com/
repository: 
issue: 
icon: com.fatbtc.FatBTCApp.jpg
bugbounty: 
meta: stale
verdict: custodial
date: 2023-07-22
signer: 
reviewArchive: 
twitter: fatbtc
social: 
features: 
developerName: FATBTC LTD

---

{% include copyFromAndroid.html %}
