---
wsId: rockwalletApp
title: 'RockWallet: Buy and Swap'
altTitle: 
authors:
- danny
appId: com.rockwallet.app
appCountry: us
idd: '6444194230'
released: 2022-11-16
updated: 2023-10-13
version: 5.11.0
stars: 4.7
reviews: 135
size: '122590208'
website: https://www.rockwallet.com
repository: 
issue: https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/498
icon: com.rockwallet.app.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2023-08-28
signer: 
reviewArchive: 
twitter: rockwallet
social:
- https://www.facebook.com/rockwalletofficial
- https://www.instagram.com/rockwallet
- https://www.linkedin.com/company/rockwallet
features: 
developerName: RockWallet, LLC

---

{% include copyFromAndroid.html %}
