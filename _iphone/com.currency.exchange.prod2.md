---
wsId: 
title: Dzengi.com - Crypto Exchange
altTitle: 
authors:
- danny
appId: com.currency.exchange.prod2
appCountry: by
idd: 1458917114
released: 2019-04-23
updated: 2023-10-24
version: 1.42.0
stars: 4.8
reviews: 3479
size: '92188672'
website: https://currency.com/
repository: 
issue: 
icon: com.currency.exchange.prod2.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2022-01-09
signer: 
reviewArchive: 
twitter: currencycom
social:
- https://www.facebook.com/currencycom
- https://www.reddit.com/r/currencycom
features: 
developerName: DI INVESTMENTS LLC

---

<!--
  According to the Android review, this app was falsely marked as wsId currencycominvesting and needs another close look.
-->
