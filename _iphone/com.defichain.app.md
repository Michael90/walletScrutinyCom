---
wsId: DeFiChainWallet
title: DeFiChain Wallet
altTitle: 
authors:
- danny
appId: com.defichain.app
appCountry: qa
idd: 1572472820
released: 2021-08-18
updated: 2023-11-07
version: 2.29.0
stars: 0
reviews: 0
size: '34121728'
website: https://defichain.com/
repository: 
issue: 
icon: com.defichain.app.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-11-11
signer: 
reviewArchive: 
twitter: defichain
social:
- https://www.linkedin.com/company/defichain
- https://www.facebook.com/defichain.official
- https://www.reddit.com/r/defiblockchain
features: 
developerName: Cake DeFi

---

{% include copyFromAndroid.html %}
