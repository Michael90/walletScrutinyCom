---
wsId: Tokenize
title: Tokenize Xchange
altTitle: 
authors:
- danny
appId: com.tokenize.exchange.trading
appCountry: us
idd: 1495765876
released: 2020-02-02
updated: 2023-04-18
version: 2.0.16
stars: 0
reviews: 0
size: '82540544'
website: https://tokenize.exchange
repository: 
issue: 
icon: com.tokenize.exchange.trading.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-08
signer: 
reviewArchive: 
twitter: TokenizeXchange
social:
- https://www.facebook.com/tokenize.exchange
features: 
developerName: Tokenize Xchange Pte Ltd

---

{% include copyFromAndroid.html %}
