---
wsId: bitkapAfrica
title: Bitkap
altTitle: 
authors:
- danny
appId: net.bitkap.frondend
appCountry: fr
idd: '1556696679'
released: 2021-04-06
updated: 2023-07-25
version: 2.1.10
stars: 4.5
reviews: 22
size: '45794304'
website: 
repository: 
issue: 
icon: net.bitkap.frondend.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-03
signer: 
reviewArchive: 
twitter: BitkapO
social:
- https://bitkap.africa
- https://www.linkedin.com/company/limitlesstech237
- https://www.facebook.com/BitkapAfrica
- https://t.me/joinchat/UKx9cqSCN9LBbH34
features: 
developerName: REAL TIME SARL

---

{% include copyFromAndroid.html %}
