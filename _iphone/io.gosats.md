---
wsId: gosats
title: 'GoSats: Bitcoin Rewards App'
altTitle: 
authors:
- danny
appId: io.gosats
appCountry: in
idd: '1536263998'
released: 2021-01-05
updated: 2023-10-03
version: 2.3.11
stars: 4.2
reviews: 235
size: '63683584'
website: https://gosats.io
repository: 
issue: 
icon: io.gosats.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-03-23
signer: 
reviewArchive: 
twitter: gosatsapp
social: 
features: 
developerName: Saffron Technologies Pte Ltd

---

{% include copyFromAndroid.html %}