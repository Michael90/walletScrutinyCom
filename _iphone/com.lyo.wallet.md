---
wsId: lyowallet
title: 'LYO WALLET: NFT''s & Crypto'
altTitle: 
authors:
- danny
appId: com.lyo.wallet
appCountry: ae
idd: '1624892548'
released: 2022-06-07
updated: 2023-05-23
version: 1.4.2
stars: 5
reviews: 1
size: '50787328'
website: https://wallet.lyofi.com/
repository: 
issue: 
icon: com.lyo.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-04-28
signer: 
reviewArchive: 
twitter: lyopayofficial
social:
- https://www.facebook.com/lyopayofficial
- https://www.instagram.com/lyopayofficial/
- https://t.me/lyopay
- https://www.linkedin.com/company/lyopay/
features: 
developerName: DIGILYO APP LTD

---

{% include copyFromAndroid.html %}
