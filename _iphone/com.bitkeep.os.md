---
wsId: bitkeep
title: Bitget Wallet, ex BitKeep
altTitle: 
authors:
- leo
appId: com.bitkeep.os
appCountry: 
idd: 1395301115
released: 2018-09-26
updated: 2023-11-06
version: 8.5.1
stars: 4.4
reviews: 461
size: '170438656'
website: https://bitkeep.com
repository: 
issue: 
icon: com.bitkeep.os.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive: 
twitter: BitKeepOS
social:
- https://www.facebook.com/bitkeep
- https://github.com/bitkeepcom
features: 
developerName: BitKeep Global Inc.

---

 {% include copyFromAndroid.html %}
