---
wsId: swanBitcoin
title: Swan Bitcoin
altTitle: 
authors:
- danny
appId: com.swanbitcoin.app
appCountry: us
idd: '1576287352'
released: 2022-08-25
updated: 2023-11-07
version: 1.4.5
stars: 4.9
reviews: 504
size: '116320256'
website: http://www.swanbitcoin.com
repository: 
issue: 
icon: com.swanbitcoin.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-17
signer: 
reviewArchive: 
twitter: swan
social:
- https://www.instagram.com/swanbitcoin
- https://www.linkedin.com/company/swanbitcoin
features: 
developerName: Swan Bitcoin

---

{% include copyFromAndroid.html %}
