---
wsId: ari10Exchange
title: Ari10 Exchange
altTitle: 
authors:
- danny
appId: com.ari10.bitcan
appCountry: pl
idd: '1623342435'
released: 2022-07-19
updated: 2023-10-15
version: 1.1.8
stars: 4.4
reviews: 13
size: '98157568'
website: https://ari10.com/exchange
repository: 
issue: 
icon: com.ari10.bitcan.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-09-07
signer: 
reviewArchive: 
twitter: ari10com
social:
- https://www.instagram.com/senexpay
- https://www.linkedin.com/company/ari10-com
- https://t.me/ari10_com
features: 
developerName: BITCAN sp. z o.o.

---

{% include copyFromAndroid.html %}