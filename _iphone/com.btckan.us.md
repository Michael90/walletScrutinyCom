---
wsId: BitKan
title: 'BitKan: Trade Bitcoin & Crypto'
altTitle: 
authors:
- danny
appId: com.btckan.us
appCountry: us
idd: 1004852205
released: 2015-06-24
updated: 2023-10-25
version: 8.17.1
stars: 3.8
reviews: 32
size: '147384320'
website: https://bitkan.com/
repository: 
issue: 
icon: com.btckan.us.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive: 
twitter: bitkanofficial
social: 
features: 
developerName: BITBANG LIMITED

---

{% include copyFromAndroid.html %}
