---
wsId: banosuperapp
title: Bano - Connect Your Life
altTitle: 
authors:
- danny
appId: com.banofinancial.app
appCountry: au
idd: '1562849570'
released: 2021-05-13
updated: 2023-10-27
version: 1.5.13
stars: 4.1
reviews: 46
size: '104997888'
website: https://bano.app
repository: 
issue: 
icon: com.banofinancial.app.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-03-09
signer: 
reviewArchive: 
twitter: banosuperapp
social:
- https://www.facebook.com/banosuperapp
- https://www.instagram.com/banosuperapp
- https://www.linkedin.com/company/banoapp/
features: 
developerName: Bano Pty Ltd

---

{% include copyFromAndroid.html %}

