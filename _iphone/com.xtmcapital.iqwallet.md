---
wsId: IQWalletCryptoWallet
title: IQ Crypto
altTitle: 
authors:
- danny
appId: com.xtmcapital.iqwallet
appCountry: ru
idd: 1589400699
released: 2021-10-18
updated: 2021-10-19
version: '1.3'
stars: 4.7
reviews: 3
size: '19799040'
website: https://iqwallet.io/
repository: 
issue: 
icon: com.xtmcapital.iqwallet.jpg
bugbounty: 
meta: obsolete
verdict: obfuscated
date: 2023-10-09
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: XTM Capital Ltd

---

{% include copyFromAndroid.html %}
