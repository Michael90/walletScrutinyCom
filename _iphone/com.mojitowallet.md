---
wsId: mojitoWallet
title: Mojito wallet
altTitle: 
authors:
- danny
appId: com.mojitowallet
appCountry: us
idd: '1620691992'
released: 2022-07-15
updated: 2023-05-19
version: 0.1.6
stars: 5
reviews: 1
size: '48752640'
website: https://www.mintlayer.org/
repository: 
issue: 
icon: com.mojitowallet.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2023-09-06
signer: 
reviewArchive: 
twitter: mintlayer
social:
- https://t.me/mintlayer
- https://www.linkedin.com/company/mintlayer
- https://discord.gg/gkZ4h8McBT
- https://www.facebook.com/MintlayerOfficial
- https://www.youtube.com/channel/UCVVpaPry8xZS47pPBmS4rnA/videos
features: 
developerName: RBB SRL

---

{% include copyFromAndroid.html %}