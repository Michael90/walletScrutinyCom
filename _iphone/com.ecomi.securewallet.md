---
wsId: ECOMIsecurewallet
title: ECOMI Secure Wallet
altTitle: 
authors:
- danny
appId: com.ecomi.securewallet
appCountry: 
idd: '1432655565'
released: 2018-08-25
updated: 2023-09-25
version: 3.0.6
stars: 4.4
reviews: 55
size: '40947712'
website: https://wallet.ecomi.com/
repository: 
issue: 
icon: com.ecomi.securewallet.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-10-03
signer: 
reviewArchive: 
twitter: ecomi_
social: 
features: 
developerName: ECOMI Technology PTE Ltd

---

{% include copyFromAndroid.html %}