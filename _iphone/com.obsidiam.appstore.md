---
wsId: obsidiam
title: Obsidiam
altTitle: 
authors:
- danny
appId: com.obsidiam.appstore
appCountry: us
idd: '1449766297'
released: 2020-09-03
updated: 2023-05-08
version: 2.9.0
stars: 4.9
reviews: 14
size: '80191488'
website: 
repository: 
issue: 
icon: com.obsidiam.appstore.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-05-31
signer: 
reviewArchive: 
twitter: obsidiamlatam
social:
- https://www.facebook.com/obsidiamexchange
- https://www.youtube.com/channel/UC67iQdeNtxzRSEZu76cybQQ
- https://www.instagram.com/obsidiam
- https://t.me/obsidiam
features: 
developerName: Obsidiam

---

{% include copyFromAndroid.html %}