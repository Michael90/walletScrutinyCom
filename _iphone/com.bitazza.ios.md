---
wsId: bitazza
title: 'Bitazza TH: Crypto Exchange'
altTitle: 
authors:
- danny
appId: com.bitazza.ios
appCountry: th
idd: 1476944844
released: 2020-05-25
updated: 2023-10-16
version: 3.3.5
stars: 3.8
reviews: 726
size: '169927680'
website: https://www.bitazza.com
repository: 
issue: 
icon: com.bitazza.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: bitazzaofficial
social:
- https://www.linkedin.com/company/bitazza
- https://www.facebook.com/bitazza
features: 
developerName: Bitazza Company Limited

---

{% include copyFromAndroid.html %}
