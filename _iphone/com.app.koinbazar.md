---
wsId: koinbazar
title: KoinBX
altTitle: 
authors:
- danny
appId: com.app.koinbazar
appCountry: in
idd: 1567360326
released: 2021-06-02
updated: 2023-10-25
version: '3.1'
stars: 2.8
reviews: 93
size: '67854336'
website: https://www.koinbx.com/
repository: 
issue: 
icon: com.app.koinbazar.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive: 
twitter: koinbazar
social:
- https://www.linkedin.com/company/koinbazar
- https://www.facebook.com/koinbazar
features: 
developerName: KOOZ ADVISORS AND TECHNOLOGIES PRIVATE LIMITED

---

{% include copyFromAndroid.html %}
