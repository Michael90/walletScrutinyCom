---
wsId: junoFinance
title: Juno - Save and Invest
altTitle: 
authors:
- danny
appId: com.capitalJ.onJuno
appCountry: us
idd: '1525858971'
released: 2021-02-13
updated: 2023-11-03
version: 3.2.6
stars: 3.9
reviews: 823
size: '410243072'
website: https://juno.finance
repository: 
issue: 
icon: com.capitalJ.onJuno.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-19
signer: 
reviewArchive: 
twitter: JunoFinanceHQ
social:
- https://www.linkedin.com/company/junofinancehq
features: 
developerName: CapitalJ Inc

---

{% include copyFromAndroid.html %}
