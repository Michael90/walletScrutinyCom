---
wsId: cointreeMobile
title: Cointree - Buy Bitcoin Easily
altTitle: 
authors:
- danny
appId: com.cointree.mobileapp
appCountry: au
idd: '1636243621'
released: 2022-07-27
updated: 2023-09-05
version: 1.2.5
stars: 3.8
reviews: 20
size: '131292160'
website: https://www.cointree.com
repository: 
issue: 
icon: com.cointree.mobileapp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-09-06
signer: 
reviewArchive: 
twitter: CointreeAus
social:
- https://www.linkedin.com/company/cointree
- https://www.instagram.com/cointreeofficial
- https://www.facebook.com/cointreeofficial
- https://www.youtube.com/channel/UCLalEIAi3vO59xscJarTtqQ
features: 
developerName: Cointree

---

{% include copyFromAndroid.html %}