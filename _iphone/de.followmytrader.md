---
wsId: followMyTrader
title: Follow MyTrader
altTitle: 
authors:
- danny
appId: de.followmytrader
appCountry: us
idd: '1487657162'
released: 2020-03-06
updated: 2023-11-08
version: 2.4.2
stars: 0
reviews: 0
size: '111078400'
website: https://followmymoney.de
repository: 
issue: 
icon: de.followmytrader.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-13
signer: 
reviewArchive: 
twitter: 
social:
- https://www.facebook.com/FollowMyMoneyOfficial
- https://www.instagram.com/followmymoney_official
- https://www.youtube.com/c/FollowMyMoneyYT
- https://www.linkedin.com/company/fels-group/
features: 
developerName: FELS Group GmbH

---

{% include copyFromAndroid.html %}

