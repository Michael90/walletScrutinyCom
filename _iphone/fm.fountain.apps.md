---
wsId: fountainPodcasts
title: Fountain Podcasts
altTitle: 
authors:
- danny
appId: fm.fountain.apps
appCountry: ph
idd: '1576394424'
released: 2021-07-28
updated: 2023-10-26
version: 0.8.6
stars: 0
reviews: 0
size: '72413184'
website: https://www.fountain.fm
repository: 
issue: 
icon: fm.fountain.apps.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-24
signer: 
reviewArchive: 
twitter: fountain_app
social: 
features: 
developerName: Fountain Labs Ltd.

---

{% include copyFromAndroid.html %}