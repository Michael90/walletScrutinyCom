---
wsId: roseon
title: Roseon
altTitle: 
authors:
- danny
appId: com.roseon.finance.production
appCountry: vn
idd: 1559440997
released: 2021-05-24
updated: 2023-11-06
version: 2.4.15
stars: 4.7
reviews: 40
size: '112968704'
website: https://roseon.finance/
repository: 
issue: 
icon: com.roseon.finance.production.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: RoseonFinance
social:
- https://www.facebook.com/Roseon.Finance
features: 
developerName: MYBITOK OU

---

{% include copyFromAndroid.html %}
