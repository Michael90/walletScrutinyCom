---
wsId: sonicWallet
title: 'Sonic Wallet : Future of Money'
altTitle: 
authors:
- danny
appId: com.sonicwallet
appCountry: us
idd: '1626223788'
released: 2022-08-28
updated: 2023-09-03
version: '3.0'
stars: 5
reviews: 2
size: '68672512'
website: 
repository: 
issue: 
icon: com.sonicwallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-08-30
signer: 
reviewArchive: 
twitter: SonicWalletHQ
social:
- https://www.sonicwallet.com
- https://www.facebook.com/sonicwallet
- https://www.linkedin.com/company/sonicwallet
- https://www.instagram.com/sonicwallet
- https://www.youtube.com/channel/UCilya9JzFKcTxk1wOPIyH2A
features: 
developerName: Sonic Wallet

---

{% include copyFromAndroid.html %}