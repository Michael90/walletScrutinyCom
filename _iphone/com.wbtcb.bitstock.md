---
wsId: bitPlusbyWBTCb
title: Bit.plus by wBTCb
altTitle: 
authors:
- danny
appId: com.wbtcb.bitstock
appCountry: us
idd: '1508577020'
released: 2020-06-22
updated: 2023-09-27
version: 2.6.2
stars: 5
reviews: 2
size: '62352384'
website: https://bit.plus
repository: 
issue: 
icon: com.wbtcb.bitstock.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2023-04-13
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: IP wBTCB solutions, s.r.o.

---

{% include copyFromAndroid.html %}

