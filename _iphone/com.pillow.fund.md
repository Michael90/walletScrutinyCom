---
wsId: pillowFund
title: 'Pillow Fund: Invest in Dollars'
altTitle: 
authors:
- danny
appId: com.pillow.fund
appCountry: in
idd: '1593227237'
released: 2022-03-02
updated: 2023-08-01
version: '5.0'
stars: 3.8
reviews: 61
size: '51591168'
website: https://www.pillow.fund/
repository: 
issue: 
icon: com.pillow.fund.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-06-13
signer: 
reviewArchive: 
twitter: PillowFund
social:
- https://www.linkedin.com/company/pillowfund
features: 
developerName: Pillow Digital Technologies Pte Ltd

---

{% include copyFromAndroid.html %}