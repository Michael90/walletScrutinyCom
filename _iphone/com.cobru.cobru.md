---
wsId: cobru
title: Cobru
altTitle: 
authors:
- danny
appId: com.cobru.cobru
appCountry: us
idd: '1574045983'
released: 2021-06-29
updated: 2023-10-26
version: 4.0.98
stars: 5
reviews: 2
size: '59667456'
website: https://cobru.co
repository: 
issue: 
icon: com.cobru.cobru.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-03-30
signer: 
reviewArchive: 
twitter: cobruapp
social:
- https://www.facebook.com/cobruapp
- https://www.instagram.com/cobruapp/
features: 
developerName: Cobru S.A.S.

---

{% include copyFromAndroid.html %}
