---
wsId: margex100X
title: Margex – Up to 100x Leverage
altTitle: 
authors:
- danny
appId: com.margex.mobile
appCountry: us
idd: '1607974744'
released: 2022-02-09
updated: 2023-10-13
version: 4.1.4
stars: 3.7
reviews: 16
size: '64014336'
website: 
repository: 
issue: 
icon: com.margex.mobile.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-08
signer: 
reviewArchive: 
twitter: margexcom
social:
- https://margex.com
- https://www.facebook.com/margexcom
- https://www.youtube.com/c/margex
- https://t.me/margex_official
features: 
developerName: Margex Trading Solutions LTD

---

{% include copyFromAndroid.html %}
