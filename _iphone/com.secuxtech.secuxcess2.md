---
wsId: secuXMobile
title: SecuX Mobile
altTitle: 
authors:
- danny
appId: com.secuxtech.secuxcess2
appCountry: tc
idd: '1477437607'
released: 2019-09-12
updated: 2023-11-01
version: 1.21.2
stars: 0
reviews: 0
size: '40388608'
website: https://www.secuxtech.com
repository: 
issue: 
icon: com.secuxtech.secuxcess2.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-08-24
signer: 
reviewArchive: 
twitter: SecuXwallet
social:
- https://www.linkedin.com/company/secuxtech
- https://www.facebook.com/secuxtech
features: 
developerName: SecuX Technology Inc.

---

{% include copyFromAndroid.html %}